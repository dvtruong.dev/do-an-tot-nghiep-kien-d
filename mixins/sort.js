/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

export const SortHandler = {
  data () {
    return {
      sortId: null,
      sort: null,
      sortType: null
    }
  },

  computed: {
    /**
     * Get sort query
     *
     * @param {Object} Sort query
     */
    sortQuery () {
      return {
        sortId: this.sortId,
        sort: this.sort,
        sortType: this.sortType
      }
    }
  },

  created () {
    // Set sort id when url has sortId param
    if (this.$route.query.sortId) {
      this.setSort(+this.$route.query.sortId)
    }

    this.setSortQueryToUrl()
  },

  methods: {
    /**
     * Set sort
     *
     * @param {Object} id - Sort Id
     */
    setSort (id) {
      if (!id) {
        this.sortId = null
        this.sort = null
        this.sortType = null
      }

      const currentSort = this.SORT_LIST.find(item => item.id === id)

      if (currentSort) {
        this.sortId = currentSort.id
        this.sort = currentSort.sort
        this.sortType = currentSort.sortType
      }
      this.$debugger.log(this.sortId + ', ' + this.sort + ', ' + this.sortType, 'sortId, sort, sortType ->')

      this.setSortQueryToUrl()
    },

    /**
     * Set sort query to Url
     */
    setSortQueryToUrl () {
      let query = {}

      if (
        this.sortQuery.sortId
      ) {
        query = Object.assign({}, this.$route.query, {
          sortId: (this.sortQuery.sortId).toString(),
          sort: (this.sortQuery.sort).toString(),
          sortType: (this.sortQuery.sortType).toString()
        })
      } else {
        query = Object.assign({}, this.$route.query)

        delete query.sortId
        delete query.sort
        delete query.sortType
      }

      if (JSON.stringify(query) !== JSON.stringify(this.$route.query)) {
        this.$router.replace({ query })
      }
    },

    /**
     * Sort data
     *
     * @param {Number} id - Sort id from sort select
     */
    onSort (id) {
      this.setSort(id)
      this.refresh()
    }
  }
}

export default SortHandler
