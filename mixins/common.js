/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import { get } from 'lodash'

export default {

  methods: {
    /**
     * Showing error
     */
    showError (error, code = 'SOMETHING_WENT_WRONG', params = {}) {
      const message = get(error, 'response.data.message', this.$text.error(code, params))
      this.$toast.error(message)
    }
  }
}
