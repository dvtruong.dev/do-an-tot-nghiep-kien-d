/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import { PAGE_SIZES } from '~/constants'

const Paginator = {
  data () {
    return {
      limit: PAGE_SIZES[0],
      page: 1
    }
  },

  computed: {
    /**
     * Get pagination query
     *
     * @param {Object} Pagination query
     */
    paginationQuery () {
      return {
        limit: this.limit,
        page: this.page
      }
    }
  },

  created () {
    // Set page when url has page query param
    if (this.$route.query.page) {
      this.setPage(+this.$route.query.page)
    }

    // Set page limit when url has page lime query param
    if (this.$route.query.limit) {
      this.setLimit(+this.$route.query.limit)
    }

    // Set pagination query to Url
    this.setPaginationQueryToUrl()
  },

  methods: {
    /**
     * Set page
     *
     * @param {Number} page - Page number
     */
    setPage (page) {
      if (page <= 0) {
        this.$debugger.log(page, 'Invalid page value, will be ignored.')
        return
      }

      this.$debugger.log(page, 'Page ->')
      this.page = page

      this.setPaginationQueryToUrl()
    },

    /**
     * Set page limit
     *
     * @param {Number} page - Page limit number
     */
    setLimit (limit) {
      if (!PAGE_SIZES.includes(limit)) {
        this.$debugger.log(limit, 'Invalid limit value, will be ignored.')
        return
      }

      this.$debugger.log(limit, 'Limit ->')
      this.page = 1
      this.limit = limit

      this.setPaginationQueryToUrl()
    },

    /**
     * Set pagination query to Url
     */
    setPaginationQueryToUrl () {
      const query = Object.assign({}, this.$route.query, {
        limit: (this.paginationQuery.limit).toString(),
        page: (this.paginationQuery.page).toString()
      })

      if (JSON.stringify(query) !== JSON.stringify(this.$route.query)) {
        this.$router.replace({ query })
      }
    }
  }
}

export { Paginator }
export default Paginator
