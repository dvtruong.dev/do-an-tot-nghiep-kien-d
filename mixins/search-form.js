/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

export default {
  data () {
    return {
      resourceTypeName: null,
      loading: false
    }
  },

  computed: {
    /**
     * Get query
     */
    query () {
      return Object.assign({}, this.paginationQuery, this.conditionQuery)
    }
  },

  methods: {
    /**
     * Refresh to get data
     */
    refresh () {
      this.doAsync(this.fetch)
        .catch((err) => {
          console.error(err)

          this.$toast.error(
            this.$text.error('FAILED_TO_GET', { name: this.resourceTypeName })
          )
        })
    },

    createNew () {
      this.$router.push({ path: 'new', append: true })
    },

    /**
     * After the deletion confirmation dialog is confirmed
     */
    onDelete () {
      if (this.selectedId) {
        this.delete(this.selectedId)
          .then((_) => {
            this.$toast.success(
              this.$text.info('DELETED', { name: this.resourceTypeName })
            )

            this.refresh()
          })
          .catch((err) => {
            console.error(err)

            this.$toast.error(
              this.$text.error('FAILED_TO_DELETE', { name: this.resourceTypeName })
            )
          })
      }
    },

    /**
     * Search data
     *
     * @param {Number} val - Condition from search form
     */
    onSearch (val) {
      this.setPage(1)
      this.setCondition(Object.assign({}, this.defaultCondition, val))
      this.refresh()
    },

    /**
     * Change page number
     *
     * @param {Number} val - Page number
     */
    onPageChange (val) {
      this.setPage(val)
      this.refresh()
    },

    /**
     * Change page size number
     *
     * @param {Number} val - Page size number
     */
    onPageSizeChange (val) {
      this.setLimit(val)
      this.refresh()
    }
  }
}
