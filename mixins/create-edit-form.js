/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import { get } from 'lodash'

const EVENT_MODIFY = 'modify'

export default {
  data () {
    return {
      loading: false
    }
  },

  props: {
    /**
     * Model id
     */
    id: {
      type: String,
      default: null
    }
  },

  watch: {
    id (val) {
      this.getDetail(val)
    },

    model: {
      handler (val) {
        this.$debugger.log(val, 'Model')
      },
      deep: true
    }
  },

  mounted () {
    this.prepareData()
  },

  methods: {
    /**
     * Get item detail
     * @param {Number} id
     */
    getDetail (id) {
      this.loading = true
      if (id) {
        const action = this.model.detailAction
        this.$dam[action]({ id })
          .then((res) => {
            this.setModel(res.data)
          })
          .catch(err => console.error(err))
      } else {
        this.setModel()
      }
      this.loading = false
    },

    /**
     * Event trigger on Submit
     */
    async onSubmit () {
      this.loading = true
      try {
        const formData = this.model.getFormData()
        const action = this.id ? this.model.updateAction : this.model.createAction
        await this.$dam[action](formData)
        this.$toast.success(
          this.$text.info(this.id ? 'UPDATED' : 'CREATED', { name: this.modelType })
        )
        this.loading = false
        this.$emit(EVENT_MODIFY)
      } catch (err) {
        console.error(err)
        this.loading = false
        const message = get(err, 'response.data.message', null)
        if (message) {
          this.$toast.error(message)
        } else {
          this.$toast.error(
            this.$text.error(this.id ? 'FAILED_TO_UPDATE' : 'FAILED_TO_CREATE', { name: this.modelType })
          )
        }
      }
    }
  }
}
