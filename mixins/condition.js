/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import { isObject } from 'lodash'

import {
  CodecUtils,
  CODEC_JSON,
  CODEC_BASE64
} from '~/plugins/dd-api-manager/codec-utils'

const ENCODE_OPTIONS = [CODEC_JSON, CODEC_BASE64]
const DECODE_OPTIONS = ENCODE_OPTIONS.concat().reverse()

/**
 * Encode condition
 *
 * @param {Object} condition - Condition query
 * @return {String} Encoded condition
 */
export const encodeCondition = (condition) => {
  if (!isObject(condition)) {
    return null
  }

  return CodecUtils.encode(condition, ENCODE_OPTIONS)
}

/**
 * Decode condition
 *
 * @param {Object} condition - Condition query
 * @return {String} Decoded condition
 */
export const decodeCondition = (condition) => {
  return CodecUtils.decode(condition, DECODE_OPTIONS)
}

export const ConditionHandler = {
  data () {
    return {
      condition: null
    }
  },

  computed: {
    /**
     * Get condition query
     *
     * @param {Object} Condition query
     */
    conditionQuery () {
      return {
        condition: this.condition
      }
    }
  },

  created () {
    // Set condition when url has condition param
    if (this.$route.query.condition) {
      this.setCondition(this.decodeCondition(this.$route.query.condition))
    }
  },

  methods: {
    /**
     * Encode condition
     */
    encodeCondition,

    /**
     * Decode condition
     */
    decodeCondition,

    /**
     * Set condition
     *
     * @param {Object} condition - Condition
     */
    setCondition (condition) {
      if (!isObject(condition)) {
        this.$debugger.log(condition, 'Invalid condition value, will be ignored.')
        return
      }

      this.$debugger.log(condition, 'Condition ->')
      this.condition = condition

      this.setConditionQueryToUrl()
    },

    /**
     * Set condition query to Url
     */
    setConditionQueryToUrl () {
      const query = Object.assign({}, this.$route.query, {
        condition: this.encodeCondition(this.conditionQuery.condition)
      })

      if (JSON.stringify(query) !== JSON.stringify(this.$route.query)) {
        this.$router.replace({ query })
      }
    }
  }
}

export default ConditionHandler
