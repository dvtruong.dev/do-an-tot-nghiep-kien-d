/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

module.exports = {
  debug: true,

  local: true,

  // On/Off java script obfuscator
  obfuscator: false,

  siteDomain: 'localhost:3030',

  webBaseUrl: 'http://localhost:3030/',

  dam: {
    baseURL: 'http://localhost:8000/api/',
    encodeCodition: true
  }
}
