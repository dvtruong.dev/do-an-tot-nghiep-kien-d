import env from './config/env'
// import obfuscator from './config/obfuscator'

export default {
  mode: 'spa',
  /**
   * Environment variables
   */
  env,

  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    { src: '@fortawesome/fontawesome-svg-core/styles.css', lang: 'css' },
    { src: 'vue-toastification/dist/index.css', lang: 'css' },
    { src: '~/assets/scss/style.scss', lang: 'scss' }
  ],

  styleResources: {
    scss: [
      '~/assets/scss/_variables.scss',
      '~/assets/scss/_mixins.scss'
    ]
  },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/global-components' },
    { src: '~/plugins/coreui' },
    { src: '~/plugins/vue-fontawesome' },
    { src: '~/plugins/dd-api-manager' },
    { src: '~/plugins/directives' },
    { src: '~/plugins/vee-validate' },
    { src: '~/plugins/vue-toastification' },
    { src: '~/plugins/text-utils' },
    { src: '~/plugins/text-mask' },
    { src: '~/plugins/moment' },
    { src: '~/plugins/debugger' },
    { src: '~/plugins/vuex' }
  ],

  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    'cookie-universal-nuxt'
  ],

  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
    // plugins: [].concat(
    //   // add for java script obfuscator
    //   obfuscator
    // )
  }
}
