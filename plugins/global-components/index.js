/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import Vue from 'vue'

import AppUserPage from '~/components/molecules/AppUserPage'

/**
 * Organisms
 */
Vue.component('app-user-page', AppUserPage)
