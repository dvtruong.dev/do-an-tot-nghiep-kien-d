/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import Vue from 'vue'
import MaskedInput from 'vue-text-mask'

Vue.component('masked-input', MaskedInput)
