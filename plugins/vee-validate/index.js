/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import Vue from 'vue'
import { ValidationObserver, ValidationProvider, extend } from 'vee-validate'
import {
  required,
  email,
  confirmed,
  regex,
  numeric
} from 'vee-validate/dist/rules'

extend('email', {
  ...email,
  message: 'Not valid email'
})

extend('required', {
  ...required,
  message: 'This field is required'
})

extend('confirmed', {
  ...confirmed,
  message: 'Not matched'
})

extend('regex', {
  ...regex,
  message: 'Invalid format'
})

extend('ipAddress', {
  validate (value) {
    if (value.length <= 15) {
      // MAC Address
      return /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/.test(value)
    } else {
      // IP Address
      return /^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$/.test(value)
    }
  },
  message: 'Invalid format'
})

extend('numeric', {
  ...numeric
})

extend('float', {
  validate (value, args) {
    const str = value.split('.')
    if (str && str.length && str.length <= 1) {
      return parseInt(str.length) <= parseInt(args.step)
    } else {
      return parseInt(str[1].length) <= parseInt(args.step)
    }
  },
  params: ['step'],
  message: 'Max excute type float. Try with max step is 0.01'
})

extend('after', {
  params: ['target', 'name'],
  validate (value, { target }) {
    return value >= target
  },
  message: 'This field value must be equal or after {name}'
})

// Register it globally
Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)

export default (context, inject) => {
  inject('validator', ValidationProvider)
}
