/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import TextUtils from './text-utils'

export default ({ app, store }, inject) => {
  inject('text', TextUtils)
}
