/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import {
  ERROR_MESSAGE,
  CONFIRMATION_MESSAGE,
  INFORMATION_MESSAGE
} from '~/constants/messages'

/**
 * Returns a string that binds the parameter value to the message template
 *
 * @param {String} message - Message template
 * @param {Object} params - Parameter to bind
 * @return {String} message - Message
 */
const bindParams = (message, params) => {
  const messageVars = message.match(/{(.+?)}/g)
  let msg = message

  if (messageVars) {
    messageVars.forEach((messageVar) => {
      const prop = messageVar.replace(/{|}/g, '')

      if (!Object.prototype.hasOwnProperty.call(params, prop)) {
        throw new Error(`Missing required parameters: '${prop}'`)
      }

      msg = msg.replace(messageVar, params[prop])
    })
  }

  return msg
}

const TextUtils = {
  /**
   * Returns the error message with the specified name
   *
   * @param {String} code - Message code
   * @param {Object} params - Parameter to bind
   * @return {String} message - Message
   */
  error (name, params = {}) {
    const err = ERROR_MESSAGE.find(v => v.name === name)

    if (err) {
      return bindParams(err.message, params)
    }

    return ''
  },

  /**
   * Returns a confirmation message for the specified name
   *
   * @param {String} code - Message code
   * @param {Object} params - Parameter to bind
   * @return {String} message - Message
   */
  confirm (name, params = {}) {
    const cfm = CONFIRMATION_MESSAGE.find(v => v.name === name)

    if (cfm) {
      return bindParams(cfm.message, params)
    }

    return ''
  },

  /**
   * Returns an information message with the specified name
   *
   * @param {String} code - Message code
   * @param {Object} params - Parameter to bind
   * @return {String} message - Message
   */
  info (name, params = {}) {
    const info = INFORMATION_MESSAGE.find(v => v.name === name)

    if (info) {
      return bindParams(info.message, params)
    }

    return ''
  },

  /**
   * Get message from message code and return
   *
   * @param {String} code - Message code
   * @param {Object} params - Parameter to bind
   * @return {String} message - Message
   */
  code (code, params = {}) {
    const MESSAGE = [].concat(
      ERROR_MESSAGE,
      CONFIRMATION_MESSAGE,
      INFORMATION_MESSAGE
    )
    const msg = MESSAGE.find(v => v.code === code)

    if (msg) {
      return bindParams(msg.message, params)
    }

    return ''
  }
}

export default TextUtils
