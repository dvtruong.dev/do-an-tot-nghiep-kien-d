/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { iconsSet as icons } from '~/assets/icons/icons.js'

config.autoAddCss = false

library.add(icons)

export default () => {
  Vue.component('font-awesome-icon', FontAwesomeIcon)
}
