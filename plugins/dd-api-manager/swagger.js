/**
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 *
 * Rules of define operationId CRUD API
 *
 * Main: GET LIST
 * Rule: get{ModuleName}List
 * Example: 'getUserList'
 *
 * Main: POST
 * Rule: create{module_name}
 * Example: 'createUser'
 *
 * Main: GET DETAIL
 * Rule: get{module_name}
 * Example: 'getUser'
 *
 * Main: PUT
 * Rule: update{module_name}
 * Example: 'updateUser'
 *
 * Main: DELETE
 * Rule: delete{module_name}
 * Example: 'deleteUser'
 */

export const swagger = {
  paths: {
    // Upload File API
    '/upload': {
      post: {
        operationId: 'uploadFile',
        consumes: ['multipart/form-data']
      }
    },

    // Login, logout API
    '/login': {
      post: {
        operationId: 'login',
        consumes: ['application/json'],
        produces: ['application/json']
      }
    },
    '/logout': {
      post: {
        operationId: 'logout',
        consumes: ['application/json'],
        produces: ['application/json']
      }
    },

    // Regist member API
    '/register': {
      post: {
        operationId: 'createMember'
      }
    },
    // Contact
    '/contact': {
      post: {
        operationId: 'sendContact',
        consumes: ['application/json'],
        produces: ['application/json']
      }
    },

    // Box API
    '/box': {
      get: {
        operationId: 'getBoxList'
      }
    },

    // Follow API
    '/follow': {
      post: {
        operationId: 'flowBox'
      }
    },

    // Bid API
    '/bid': {
      get: {
        operationId: 'getBidList'
      },
      post: {
        operationId: 'setBid',
        consumes: ['application/json'],
        produces: ['application/json']
      }
    },

    // Product API
    '/product': {
      get: {
        operationId: 'getProductList',
        produces: ['application/json']
      }
    },
    '/product/{id}': {
      get: {
        operationId: 'getItemDetail',
        consumes: ['application/json'],
        produces: ['application/json']
      }
    },
    '/note': {
      post: {
        operationId: 'setMemo',
        consumes: ['application/json'],
        produces: ['application/json']
      }
    },

    // Notification
    '/notification': {
      get: {
        operationId: 'getNotificationList'
      }
    }
  }
}
