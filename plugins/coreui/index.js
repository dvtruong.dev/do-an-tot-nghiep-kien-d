/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import Vue from 'vue'
import CoreuiVue from '@coreui/vue'

export default () => {
  Vue.use(CoreuiVue)
}
