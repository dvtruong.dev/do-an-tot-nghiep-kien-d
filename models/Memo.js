import { get } from 'lodash'

export default class Memo {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.content = get(props, 'content', '')
  }
}
