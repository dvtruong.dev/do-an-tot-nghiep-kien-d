import { get } from 'lodash'

export default class Group {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.name = get(props, 'name', '')
    this.zone = get(props, 'zone.id', null)
    this.description = get(props, 'description', null)
    this.status = 0
    // action
    this.createAction = 'createGroup'
    this.detailAction = 'getGroup'
    this.updateAction = 'updateGroup'
  }

  /**
   * Get form data
   */
  getFormData () {
    return {
      id: this.id,
      name: this.name,
      zone_id: this.zone,
      description: this.description,
      status: this.status
    }
  }
}
