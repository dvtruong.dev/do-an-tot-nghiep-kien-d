import { get } from 'lodash'

export default class Box {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.name = get(props, 'name', '')
    this.status = get(props, 'status', 0)
    this.deadline = get(props, 'deadline', 0)
  }
}
