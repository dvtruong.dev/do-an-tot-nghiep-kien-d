import { get } from 'lodash'

export default class User {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.name = get(props, 'name', '')
    this.description = get(props, 'description', null)
    this.status = 0

    // action
    this.createAction = 'createZone'
    this.detailAction = 'getZone'
    this.updateAction = 'updateZone'
  }

  /**
   * Get form data
   */
  getFormData () {
    return {
      id: this.id,
      name: this.name,
      description: this.description,
      status: this.status
    }
  }
}
