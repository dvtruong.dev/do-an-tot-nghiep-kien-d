import { get } from 'lodash'

export default class Schedule {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.name = get(props, 'name', '')
    this.description = get(props, 'description', null)
    this.status = get(props, 'status', 0)
    this.days_of_week = this.getDaysOfWeek(get(props, 'days_of_week', ''))
    this.date_on = this.getDateBySecond(get(props, 'date_on', null))
    this.date_off = this.getDateBySecond(get(props, 'date_off', null))
    this.start_time = get(props, 'start_time', null)
    this.end_time = get(props, 'end_time', null)
    this.object = get(props, 'object', {})
    this.object_type = get(props, 'object_type', 1)
    this.object_id = get(props, 'object.id', 1)

    // append props
    this.date_range = [this.date_on, this.date_off]
    this.time_range = [this.start_time, this.end_time]

    // action
    this.createAction = 'createSchedule'
    this.detailAction = 'getSchedule'
    this.updateAction = 'updateSchedule'
  }

  /**
   * Get form data
   */
  getFormData () {
    return {
      id: this.id,
      name: this.name,
      description: this.description,
      status: this.status,
      days_of_week: this.days_of_week.map(item => item.id).join(','),
      date_on: parseInt(this.date_range[0] / 1000),
      date_off: parseInt(this.date_range[1] / 1000),
      start_time: parseInt(this.time_range[0] / 1000),
      end_time: parseInt(this.time_range[1] / 1000),
      object_id: this.object_id,
      object_type: this.object_type
    }
  }

  /**
   * Get init days of week
   * @param {String} data
   * @return {Array} days
   */
  getDaysOfWeek (data) {
    return data ? data.split(',').map((item) => {
      return {
        name: '',
        id: parseInt(item) || 0
      }
    }) : []
  }

  /**
   * Get date by second
   * @param {String} data
   * @return {Number} seconds
   */
  getDateBySecond (data) {
    return data ? data * 1000 : null
  }
}
