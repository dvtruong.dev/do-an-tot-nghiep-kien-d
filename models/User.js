import { get } from 'lodash'

export default class User {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.participation = get(props, 'participation', null)
    this.name = get(props, 'name', '')
    this.furigana = get(props, 'furigana', '')
    this.corporate_name = get(props, 'corporate_name', '')
    this.department_name = get(props, 'department_name', '')
    this.position_name = get(props, 'position_name', '')
    this.participant_number = get(props, 'participant_number', '')
    this.postal_code = get(props, 'postal_code', '')
    this.address = get(props, 'address', '')
    this.phone = get(props, 'phone', '')
    this.fax = get(props, 'fax', '')
    this.email = get(props, 'email', '')
    this.note = get(props, 'note', '')
    this.front_ivd = get(props, 'front_ivd', '')
    this.back_ivd = get(props, 'back_ivd', '')
    this.antique_license = get(props, 'antique_license', '')
    this.password = get(props, 'password', '')

    // action
    this.createAction = 'createUser'
    this.detailAction = 'getUser'
    this.updateAction = 'updateUser'
  }

  /**
   * Get form data
   */
  getFormData () {
    const data = {
      id: this.id,
      name: this.name,
      furigana: this.furigana,
      corporate_name: this.corporate_name,
      department_name: this.department_name,
      position_name: this.position_name,
      participant_number: this.participant_number,
      postal_code: this.postal_code,
      address: this.address,
      phone: this.phone,
      fax: this.fax,
      email: this.email,
      note: this.note,
      front_ivd: this.front_ivd,
      back_ivd: this.back_ivd,
      antique_license: this.antique_license,
      password: this.password

    }
    if (this.password) {
      data.password = this.password
    }
    return data
  }
}
