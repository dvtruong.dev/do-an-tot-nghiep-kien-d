import { get } from 'lodash'
import Box from '~/models/Box'
import Memo from '~/models/Memo'
import User from '~/models/User'
import Bid from '~/models/Bid'

export default class Item {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.prev_id = get(props, 'prev_id', null)
    this.next_id = get(props, 'next_id', null)
    this.name = get(props, 'name', '')
    this.box = new Box(get(props, 'box', {}))
    this.user = new User(get(props, 'user', {}))
    this.memos = get(props, 'memos', []).map(item => new Memo(item))
    this.bids = get(props, 'bids', []).map(item => new Bid(item))
    this.images = get(props, 'images', [])
    this.starting_price = get(props, 'starting_price', 0)
    this.current_price = get(props, 'current_price', 0)
    this.expected_price = get(props, 'expected_price', 0)
    this.number = get(props, 'number', 0)
    this.status = get(props, 'status', 0)
    this.note = get(props, 'note', '')
  }
}
