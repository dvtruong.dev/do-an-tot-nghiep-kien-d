/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import { get } from 'lodash'

export default class Member {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.name = get(props, 'name', null)
    this.email = get(props, 'email', null)
    this.phone = get(props, 'phone', null)
    this.participation = +get(props, 'participation', 1)
    this.furigana = get(props, 'furigana', null)
    this.corporateName = get(props, 'corporate_name', null)
    this.departmentName = get(props, 'department_name', null)
    this.positionName = get(props, 'position_name', null)
    this.participantNumber = get(props, 'participant_number', null)
    this.postalCode = get(props, 'postal_code', null)
    this.address = get(props, 'address', null)
    this.fax = get(props, 'fax', null)
    this.status = get(props, 'status', 0)
    this.note = get(props, 'note', null)
    this.frontIvd = get(props, 'front_ivd', null)
    this.backIvd = get(props, 'back_ivd', null)
    this.antiqueLicense = get(props, 'antique_license', null)

    this.frontIvdPath = get(props, 'front_path', null)
    this.backIvdPath = get(props, 'back_path', null)
    this.antiqueLicensePath = get(props, 'antiqueLicense_path', null)

    // action
    this.createAction = 'createMember'
    this.detailAction = 'getMember'
    this.updateAction = 'updateMember'
  }

  /**
   * Get form data
   */
  getFormData () {
    const data = {
      id: this.id,
      name: this.name,
      email: this.email,
      furigana: this.furigana,
      corporate_name: this.corporateName,
      department_name: this.departmentName,
      position_name: this.positionName,
      postal_code: +this.postalCode,
      address: this.address,
      phone: this.phone,
      fax: this.fax,
      note: this.note,
      participation: +this.participation
    }

    if (get(this, 'participantNumber', null)) {
      data.participant_number = +get(this, 'participantNumber', null)
    }

    if (get(this, 'frontIvd', null)) {
      data.front_ivd = +get(this, 'frontIvd', null)
    }

    if (get(this, 'backIvd', null)) {
      data.back_ivd = +get(this, 'backIvd', null)
    }

    if (get(this, 'antiqueLicense', null)) {
      data.antique_license = +get(this, 'antiqueLicense', null)
    }

    return data
  }
}
