import { get } from 'lodash'

export default class User {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.name = get(props, 'name', '')
    this.description = get(props, 'description', '')
    this.macAddress = get(props, 'mac_address', null)
    this.ipAddress = get(props, 'ip_address', null)
    this.latitude = get(props, 'latitude', null)
    this.longitude = get(props, 'latitude', null)
    this.groupId = get(props, 'group.id', null)
    this.status = 0
    this.color_r = get(props, 'color_r', 0)
    this.color_b = get(props, 'color_b', 0)
    this.color_g = get(props, 'color_g', 0)
    this.attribute = get(props, 'attribute', '')

    // action
    this.createAction = 'createThing'
    this.detailAction = 'getThing'
    this.updateAction = 'updateThing'
  }

  /**
   * Get form data
   */
  getFormData () {
    const data = {
      id: this.id,
      name: this.name,
      description: this.description,
      status: this.status,
      mac_address: this.macAddress,
      ip_address: this.ipAddress,
      color_r: this.color_r,
      color_g: this.color_g,
      color_b: this.color_b,
      latitude: this.latitude,
      longitude: this.longitude,
      attribute: this.attribute,
      group_id: this.groupId
    }
    return data
  }
}
