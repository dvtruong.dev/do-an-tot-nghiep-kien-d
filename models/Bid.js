import { get } from 'lodash'

export default class Bid {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.price = get(props, 'price', '')
  }
}
