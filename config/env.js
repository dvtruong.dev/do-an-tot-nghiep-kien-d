/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

const environment = process.env.NODE_ENV || 'local'
const baseEnv = require(`../env/${environment}`)
const env = Object.assign({ environment }, baseEnv)

module.exports = env
