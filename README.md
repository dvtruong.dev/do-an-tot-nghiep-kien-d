# Digi Dinos IoT Smart Lignth Admin

Digi Dinos IoT Smart Lignth Admin Official Website Front Application

## Constitution

Develop with `Nuxt.js + ES6 + CoreUI`.
Alt CSS uses `SCSS`.

Also, `dd-api-management` is included as a plugin as a library for call the API.

There may be a README directly under the folder, so please refer to that too.

`` `
src
 ├── assets: js / css / img etc.
 ├── components: vue components
 ├── config: Logic group used in nuxt.config.js
 ├── constants: constant management
 ├── env: Settings for each environment
 ├── layouts: Page common layout
 ├── middleware: Common page processing
 ├── mixins: Vue mixin
 ├── pages: Pages
 ├── plugins: Plugins
 ├── spec: Test file storage
 ├── static: Static file
 ├── store: vuex store
 ├── (From here on the file)
 ├── .eslintrc.js: ESLint setting
 ├── jest.config.js: Jest (test tool) configuration file
 └── nuxt.config.js: Project definition
`` `

## Build Setup

Execution is basically recommended yarn.

`` `bash
# Install dependencies
$ yarn

# Start-up
# Use the following three commands depending on the connection destination
$ yarn dev # => Connect to local local server api
$ yarn dev: local # => connect to local server api
$ yarn dev: swagger # => Connect to ua-swagger in local environment

# lint
$ yarn lint
$ yarn lint: fix # auto fix

# Build and launch
$ yarn build: local
$ yarn start

# Generate static file
$ yarn generate

# File size analysis
$ yarn analyze
`` `