/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

export default ({ store, redirect, app }) => {
  if (!app.$cookies.get('token')) {
    return redirect('/login')
  }
}
