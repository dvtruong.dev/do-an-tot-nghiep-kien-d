/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

export const ERROR_MESSAGE = [
  {
    code: 'E000',
    name: 'SOMETHING_WENT_WRONG',
    message: 'Something went wrong'
  },
  {
    code: 'E001',
    name: 'FAILED_TO_CREATE',
    message: 'Failed to create {name}'
  },
  {
    code: 'E002',
    name: 'FAILED_TO_UPDATE',
    message: 'Failed to update {name}'
  },
  {
    code: 'E003',
    name: 'FAILED_TO_DELETE',
    message: 'Failed to delete {name}'
  },
  {
    code: 'E004',
    name: 'FAILED_TO_GET',
    message: 'Failed to get {name}'
  },
  {
    code: 'E005',
    name: 'FAILED_TO_SEND_CONTACT',
    message: 'Failed to send contact'
  }
]

export const CONFIRMATION_MESSAGE = [
  {
    code: 'Q001',
    name: 'DELETE',
    message: 'Are you sure you want to delete this {name}?'
  }
]

export const INFORMATION_MESSAGE = [
  {
    code: 'I001',
    name: 'CREATED',
    message: 'Successfully created'
  },
  {
    code: 'I002',
    name: 'UPDATED',
    message: 'Successfully updated'
  },
  {
    code: 'I003',
    name: 'DELETED',
    message: 'Successfully deleted'
  },
  {
    code: 'I004',
    name: 'SENT',
    message: 'Successfully sent'
  },
  {
    code: 'I005',
    name: 'CLOSED_BID',
    message: '入札は締め切りました'
  }
]
