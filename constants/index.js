/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

/**
 * List of number record in one page
 */
export const PAGE_SIZES = [10, 20, 50]

/**
 * Max number record return by api
 */
export const MAX_LIMIT_RECORD = 1000

/**
 * Participation list
 */
export const PARTICIPATION_LIST = [
  {
    id: 1,
    name: '見学　(※見学のみの場合パドルは発行されませんので入札ができません)'
  },
  {
    id: 2,
    name: '購入　(※デポジットが発生する可能性があります)'
  },
  {
    id: 3,
    name: '出品'
  }
]

/**
 * Box status list
 */
export const BOX_STATUS_LIST = [
  {
    id: 0,
    name: '未確認'
  },
  {
    id: 1,
    name: '確認済み'
  }
]

/*
 * Item status list
 */
export const ITEM_STATUS_LIST = [
  {
    id: 1,
    name: '入札'
  },
  {
    id: 2,
    name: 'メモあり'
  },
  {
    id: 3,
    name: '別展'
  },
  {
    id: 4,
    name: 'TOP'
  },
  {
    id: 5,
    name: '自社品'
  },
  {
    id: 6,
    name: 'お気に入り'
  }
]

/**
 * Bid result status list
 */
export const BID_RESULT_STATUS_LIST = [
  {
    id: 1,
    name: '保留'
  },
  {
    id: 2,
    name: '落札'
  },
  {
    id: 3,
    name: '鉄砲'
  },
  {
    id: 4,
    name: '不成立'
  }
]

/**
 * Product result status list
 */
export const PRODUCT_RESULT_STATUS_LIST = [
  {
    id: 1,
    name: '入札有り'
  },
  {
    id: 2,
    name: '落札'
  },
  {
    id: 3,
    name: '自社品'
  }
]
