
import { MEMBER_REGISTRATION_SET_FORM_DATA } from '~/constants/mutation-types'
import Member from '~/models/Member'

export const state = () => ({
  model: new Member()
})

export const mutations = {
  /**
   * Set member regist form data
   */
  [MEMBER_REGISTRATION_SET_FORM_DATA] (state, formData) {
    Object.keys(state.model).forEach((key) => {
      if (key in formData) {
        state.model[key] = formData[key]
      }
    })
  }
}

export const getters = {
  /**
   * Input model contents
   */
  formData (state) {
    return state.model
  }
}

export const actions = {
  /**
   * Store member registration form data in store
   */
  setFormData ({ commit }, formData) {
    commit(MEMBER_REGISTRATION_SET_FORM_DATA, formData)
  }
}

export default {
  state,
  mutations,
  getters,
  actions
}
